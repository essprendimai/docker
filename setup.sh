#!/usr/bin/env bash

# Project setup
cp projects/crawler/.env.example projects/crawler/.env
docker-compose run --rm crawler bash -c "composer install --no-interaction"
docker-compose run --rm crawler bash -c "php artisan key:generate"
docker-compose run --rm crawler bash -c "php artisan migrate:fresh"
docker-compose run --rm crawler bash -c "php artisan module:migrate"
docker-compose run --rm crawler bash -c "php artisan db:seed"
docker-compose run --rm crawler bash -c "php artisan module:seed"


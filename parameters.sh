#!/usr/bin/env bash

# Base paths
PROJECTS_ROOT=projects;

# ----------------------------------------------------------------------------------------------------------------------

# Project paths
LARAVEL_DIRECTORY=${PROJECTS_ROOT}/crawler;
SUPERVISOR_DIRECTORY=${PROJECTS_ROOT}/supervisor;

# Project repositories
LARAVEL_GIT=git@bitbucket.org:essprendimai/crawler.git;
SUPERVISOR_GIT=git@bitbucket.org:essprendimai/supervisor.git;

# ----------------------------------------------------------------------------------------------------------------------